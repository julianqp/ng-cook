import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RecetaComponent } from './receta.component';

describe('FooterComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [RecetaComponent],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(RecetaComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'ng-cook'`, () => {
    const fixture = TestBed.createComponent(RecetaComponent);
    const app = fixture.componentInstance;
    expect(app.year).toEqual(2021);
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(RecetaComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain(
      'ng-cook app is running!'
    );
  });
});
