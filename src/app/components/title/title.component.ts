import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css'],
})
export class TitleComponent implements OnInit {
  recetas = 'recetas';
  ingredientes = 'ingredientes';
  subtitle: string;
  title: string;
  actualRoute;

  constructor(private router: Router) {
    if (router.url.includes('/ingredientes')) {
      this.actualRoute = 'ingredientes';
      this.title = 'Búsqueda por Ingredientes';
      this.subtitle =
        'Escriba los ingredientes que desea para obtener las recetas';
    } else {
      this.actualRoute = 'recetas';
      this.title = 'Búsqueda por recetas';
      this.subtitle = 'Escriba la receta que desea consultar.';
    }
  }

  ngOnInit() {}
}
