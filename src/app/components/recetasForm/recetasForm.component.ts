import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import axios from 'axios';

@Component({
  selector: 'app-recetasForm',
  templateUrl: './recetasForm.component.html',
  styleUrls: ['./recetasForm.component.css'],
})
export class RecetasFormComponent implements OnInit {
  posts = [];
  errors = [];
  form = new FormGroup({
    search: new FormControl('', [Validators.required]),
  });

  get f() {
    return this.form.controls;
  }

  async submit() {
    console.log('SUBMITEAMOS');
    const url = `http://www.recipepuppy.com/api/?q=${
      this.form.value.search
    }&p=${1}`;
    const response = await axios.post('https://julian-cors.herokuapp.com/', {
      url,
    });
    if (response.data && response.data.results) {
      console.log(response.data.results);
    }

    /*
    this.http
      .get('https://api.github.com/repositories')
      .then((response) => {
        this.posts = response.data.entry;
      })
      .catch((e) => {
        this.errors.push(e);
      });
      */
    console.log(this.form.value);
  }

  ngOnInit() {
    console.log('RECETAS');
  }
}
