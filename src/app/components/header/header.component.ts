import { Component } from '@angular/core';
import { faAngular } from '@fortawesome/free-brands-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  recetas = 'recetas';
  ingredientes = 'ingredientes';
  faAngular = faAngular;
  classRecetas = 'px-2 py-1';
  classIngredientes = 'px-2 py-1';

  constructor(private router: Router) {
    if (router.url.includes('/ingredientes')) {
      this.classIngredientes += '  text-blue-500';
    } else {
      this.classRecetas += '  text-blue-500';
    }
  }
}
