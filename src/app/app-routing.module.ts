import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from '../app/app.component';
import { RecetasComponent } from './components/recetas/recetas.component';
import { IngredientesComponent } from './components/ingredientes/ingredientes.component';

const routes: Routes = [
  { path: 'ingredientes', component: IngredientesComponent },
  { path: '', component: RecetasComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
